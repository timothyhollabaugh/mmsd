project(
  'mmsdtng',
  'c', 'cpp',
  version : '1.7',
  meson_version : '>= 0.56.0',
)

check_headers = [
  ['HAVE_DLFCN_H', 'dlfcn.h'],
  ['HAVE_INTTYPES_H', 'inttypes.h'],
  ['HAVE_STDINT_H', 'stdint.h'],
  ['HAVE_STDIO_H', 'stdio.h'],
  ['HAVE_STDLIB_H', 'stdlib.h'],
  ['HAVE_STRINGS_H', 'strings.h'],
  ['HAVE_STRING_H', 'string.h'],
  ['HAVE_SYS_STAT_H', 'sys/stat.h'],
  ['HAVE_SYS_TYPES_H', 'sys/types.h'],
  ['HAVE_UNISTD_H', 'unistd.h']
]

cc = meson.get_compiler('c')
conf_data = configuration_data()

foreach h : check_headers
  if cc.has_header(h.get(1))
    conf_data.set(h.get(0), 1)
  endif
endforeach
conf_data.set('VERSION', meson.project_version())
conf_data.set('NAME', meson.project_name())
mobile_broadband_provider_info_database = dependency('mobile-broadband-provider-info').get_variable(pkgconfig: 'database')
conf_data.set('MOBILE_BROADBAND_PROVIDER_INFO_DATABASE', mobile_broadband_provider_info_database)
conf_data.set('SOURCE_ROOT', meson.project_source_root())

conf = configure_file(
  input : 'config.h.in',
  output : 'config.h',
  configuration : conf_data
)

add_project_arguments('-DMMS_PLUGIN_BUILTIN', language : 'c')
add_project_arguments('-DHAVE_CONFIG_H', language : 'c')
add_project_arguments('-DPLUGINDIR="@0@/mms/plugins"'.format(get_option('libdir')), language : 'c')

includes = [
  include_directories('.'),
  include_directories('src')
]

dependencies = [
  dependency('glib-2.0', version : '>=2.16'),
  dependency('mm-glib', version : '>=1.14'),
  dependency('libsoup-2.4', version : '>=2.60'),
  dependency('libcares', version : '>1.17'),
  cc.find_library('dl'),
  cc.find_library('phonenumber', required: true)
]

subdir('src')
subdir('plugins')

mmsd = executable('mmsdtng',
  'src/main.c',
  dependencies : dependencies,
  include_directories : includes,
  install : true,
  link_with : [mms_lib, plugins_lib]
)

subdir('tools')
subdir('unit')
