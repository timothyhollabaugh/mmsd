/*
 *
 *  Multimedia Messaging Service Daemon - The Next Generation
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gio/gio.h>

#include "dbus.h"
#include "mms.h"

static GDBusConnection *connection;
static GDBusNodeInfo *introspection_data = NULL;

GDBusConnection *mms_dbus_get_connection(void)
{
	return connection;
}

void __mms_dbus_set_connection(GDBusConnection *conn)
{
	connection = conn;
}

GDBusNodeInfo *mms_dbus_get_introspection_data(void)
{
	return introspection_data;
}

void __mms_dbus_set_introspection_data(void)
{
	introspection_data = g_dbus_node_info_new_for_xml (introspection_xml, NULL);
	g_assert (introspection_data != NULL);
}

void __mms_dbus_unref_introspection_data(void)
{
	g_dbus_node_info_unref (introspection_data);
}
